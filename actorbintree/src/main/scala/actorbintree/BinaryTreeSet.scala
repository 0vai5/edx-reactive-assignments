/**
 * Copyright (C) 2009-2013 Typesafe Inc. <http://www.typesafe.com>
 */
package actorbintree

import akka.actor._
import scala.collection.immutable.Queue

object BinaryTreeSet {

  trait Operation {
    def requester: ActorRef
    def id: Int
    def elem: Int
  }

  trait OperationReply {
    def id: Int
  }

  /** Request with identifier `id` to insert an element `elem` into the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Insert(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to check whether an element `elem` is present
    * in the tree. The actor at reference `requester` should be notified when
    * this operation is completed.
    */
  case class Contains(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to remove the element `elem` from the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Remove(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request to perform garbage collection*/
  case object GC

  /** Holds the answer to the Contains request with identifier `id`.
    * `result` is true if and only if the element is present in the tree.
    */
  case class ContainsResult(id: Int, result: Boolean) extends OperationReply
  
  /** Message to signal successful completion of an insert or remove operation. */
  case class OperationFinished(id: Int) extends OperationReply

}


class BinaryTreeSet extends Actor {
  import BinaryTreeSet._
  import BinaryTreeNode._

  def createRoot: ActorRef = context.actorOf(BinaryTreeNode.props(0, initiallyRemoved = true))

  var root = createRoot

  // optional
  var pendingQueue = Queue.empty[Operation]

  // optional
  def receive = normal

  // optional
  /** Accepts `Operation` and `GC` messages. */
  val normal: Receive = {
    case o:Operation => root ! o
    case GC => val newRoot = createRoot
               root ! CopyTo(newRoot)
               context.become(garbageCollecting(newRoot))
  }

  // optional
  /** Handles messages while garbage collection is performed.
    * `newRoot` is the root of the new binary tree where we want to copy
    * all non-removed elements into.
    */
  def garbageCollecting(newRoot: ActorRef): Receive = {
    case o:Operation => pendingQueue = pendingQueue enqueue o
    case CopyFinished => root ! PoisonPill //here root and sender should be same
                         root = newRoot
                         while(pendingQueue.nonEmpty){
                           val (op, que) = pendingQueue.dequeue
                           pendingQueue = que
                           root ! op
                         }
                         context.become(normal)
    case GC => () //Ignore GC messages while GC going on
  }

}

object BinaryTreeNode {
  trait Position

  case object Left extends Position
  case object Right extends Position

  case class CopyTo(treeNode: ActorRef)
  case object CopyFinished

  def props(elem: Int, initiallyRemoved: Boolean) = Props(classOf[BinaryTreeNode],  elem, initiallyRemoved)
}

class BinaryTreeNode(val elem: Int, initiallyRemoved: Boolean) extends Actor {
  import BinaryTreeNode._
  import BinaryTreeSet._

  var subtrees = Map[Position, ActorRef]()
  var removed = initiallyRemoved

  // optional
  def receive = normal

  // optional
  /** Handles `Operation` messages and `CopyTo` requests. */
  val normal: Receive = {
    case c:Contains => search(c)
    case i:Insert => insert(i)
    case r:Remove => remove(r)
    case CopyTo(destNode) => copyTo(destNode)
  }

  def copyTo(destNode: ActorRef): Unit = {
    if(!removed){
      destNode ! Insert(self, -1, elem)
    }
    val subnodes = subtrees.values.toSet
    subnodes.foreach(_ ! CopyTo(destNode))
    if (subnodes.nonEmpty || !removed) {
      //if already removed, that means the elem in this node is not required to be inserted which is same as
      //assuming insert is confirmed for this node or insertConfirmed = true
      context.become(copying(subnodes, removed))
    }else {
      //here context.parent and sender should be same
      context.parent ! CopyFinished
    }
  }

  def insert(query: Insert): Unit = {
    def insertAt(pos: Position): Unit = {
      if (!subtrees.contains(pos)){
        subtrees += (pos -> context.actorOf(props(query.elem, false)))
        query.requester ! OperationFinished(query.id)
      } else {
        subtrees.get(pos).get ! query
      }
    }
    query.elem - elem match {
      case x if !removed && x == 0 => query.requester ! OperationFinished(query.id)
      case x if x < 0 => insertAt(Left)
      case _ => insertAt(Right)
    }
  }

  def search(query: Contains): Unit = {
    def searchAt(pos: Position): Unit = {
      if (!subtrees.contains(pos)){
        query.requester ! ContainsResult(query.id, false)
      } else {
        subtrees.get(pos).get ! query
      }
    }
    query.elem - elem match {
      case x if !removed && x == 0 => query.requester ! ContainsResult(query.id, true)
      case x if x < 0 => searchAt(Left)
      case _ => searchAt(Right)
    }
  }

  def remove(query: Remove): Unit = {
    def removeAt(pos: Position): Unit = {
      if (!subtrees.contains(pos)){
        query.requester ! OperationFinished(query.id)
      } else {
        subtrees.get(pos).get ! query
      }
    }
    query.elem - elem match {
      case x if !removed && x == 0 => removed = true; query.requester ! OperationFinished(query.id)
      case x if x < 0 => removeAt(Left)
      case _ => removeAt(Right)
    }
  }

  // optional
  /** `expected` is the set of ActorRefs whose replies we are waiting for,
    * `insertConfirmed` tracks whether the copy of this node to the new tree has been confirmed.
    */
  def copying(expected: Set[ActorRef], insertConfirmed: Boolean): Receive = {
    case OperationFinished(_) if expected.isEmpty => context.parent ! CopyFinished; context.become(normal)
    case OperationFinished(_) => context.become(copying(expected, true))
    case CopyFinished => val remaining = expected - sender
                         if(remaining.isEmpty && insertConfirmed){
                           sender ! PoisonPill
                           context.parent ! CopyFinished; context.become(normal)
                         }else{
                           context.become(copying(remaining, insertConfirmed))
                         }
  }


}
