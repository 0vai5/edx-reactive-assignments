package kvstore

import akka.actor.{ OneForOneStrategy, PoisonPill, Props, SupervisorStrategy, Terminated, ActorRef, Actor }
import kvstore.Arbiter._
import akka.pattern.{ ask, pipe }
import scala.concurrent.duration._
import akka.util.Timeout

object Replica {
  sealed trait Operation {
    def key: String
    def id: Long
  }
  case class Insert(key: String, value: String, id: Long) extends Operation
  case class Remove(key: String, id: Long) extends Operation
  case class Get(key: String, id: Long) extends Operation

  sealed trait OperationReply
  case class OperationAck(id: Long) extends OperationReply
  case class OperationFailed(id: Long) extends OperationReply
  case class GetResult(key: String, valueOption: Option[String], id: Long) extends OperationReply

  case class PersistTimeout(id: Long)
  case class RetryPersist(id: Long)

  case class GlobalAckTimeout(id: Long)

  def props(arbiter: ActorRef, persistenceProps: Props): Props = Props(new Replica(arbiter, persistenceProps))
}

class Replica(val arbiter: ActorRef, persistenceProps: Props) extends Actor {
  import Replica._
  import Replicator._
  import Persistence._
  import context.dispatcher

  override val supervisorStrategy = OneForOneStrategy(){
    case _:Exception => SupervisorStrategy.Restart
  }

  arbiter ! Join

  val persistActor = context.actorOf(persistenceProps)

  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */
  
  var kv = Map.empty[String, String]
  // the current set of replicators
  var replicators = Set.empty[ActorRef]

  // a map from secondary replicas to replicators
  // this also include primary replica as
  // my approach in the solution is that primary replica does everything
  // a secondary replica and some extra
  var secondaries = Map(self -> (makeReplicator(self)))

  var expectedSeqNum = 0L

  var retryPersist = Map.empty[Long, (ActorRef, Persist)]

  //map of id and a tuple (client, replicators) where replicators
  // are the ones which have not yet responded with Replicated
  var globalAcks = Map.empty[Long, (ActorRef, Set[ActorRef])]

  def receive = {
    case JoinedPrimary   => context.become(leader)
    case JoinedSecondary => context.become(replica)
  }

  def makeReplicator(replica: ActorRef): ActorRef = {
    val replicator = context.actorOf(Props(new Replicator(replica)))
    replicators += replicator
    replicator
  }

  /* TODO Behavior for  the leader role. */
  val leader: Receive = {
    case Replicas(allReplicas) =>
      val newSecondaries =
        for {
          replicaRef <- allReplicas
          if (!secondaries.contains(replicaRef))
        } yield (replicaRef -> makeReplicator(replicaRef))
      secondaries ++= newSecondaries
      //send old mssgs to new replicas
      //this is just dummy id
      //using negative in the hope that client won't use negative numbers
      var id = Long.MinValue
      kv foreach {
        case (k,v) =>
          newSecondaries foreach {
            case (_,replicator) =>
              replicator ! Replicate(k, Some(v), id)
              id += 1
          }}
      //disable inactive replicators
      var scndToRemove = Set.empty[ActorRef]
      var replctsToRemove = Set.empty[ActorRef]
      secondaries foreach {
        case (sec,rplctor) if !allReplicas.contains(sec) =>
          replicators -= rplctor
          replctsToRemove += rplctor
          scndToRemove += sec
          rplctor ! Replicator.CleanUp
        case _ => ()
      }
      secondaries --= scndToRemove
      //waiveoff can be done here too by traversing globalacks
      //and removing no inactive replicators
      globalAcks = for {
        (id, (client, repctrs)) <- globalAcks
      } yield (id -> (client -> (repctrs -- replctsToRemove)))

    case op@Insert(k,v,id) =>
      kv += (k -> v)
      globalAcks += id -> (sender -> replicators)
      replicators.foreach(_ ! Replicate(k, Some(v), id))
      context.system.scheduler.scheduleOnce(1150.millisecond, self, GlobalAckTimeout(id))
    case Remove(k,id) =>
      kv -= k
      globalAcks += id -> (sender -> replicators)
      replicators.foreach(_ ! Replicate(k, None, id))
      context.system.scheduler.scheduleOnce(1150.millisecond, self, GlobalAckTimeout(id))
    case Replicated(_,id) =>
      globalAcks.get(id) match {
        case Some((client,rplctrs)) =>
          globalAcks -= id
          val remaining = rplctrs - sender
          if (remaining.nonEmpty){
            globalAcks += id -> (client -> remaining)
          }else{
            client ! OperationAck(id)
          }
        case None => () //timedout
      }
    case GlobalAckTimeout(id) =>
      globalAcks.get(id) match {
        case Some((client, unacks)) =>
          if(unacks.filter(replicators.contains(_)).nonEmpty) {
            client ! OperationFailed(id)
          }else {
            client ! OperationAck(id)
          }

        case None => () //successful and already removed
      }
    case msg@_ => secondary(msg)
  }

  /* TODO Behavior for the replica role. */
  val replica: Receive = secondary

  def secondary: PartialFunction[Any, Unit] = {
    case Get(k,id) => sender ! GetResult(k, kv.get(k), id)
    case Snapshot(k,ov,seq) =>
      if (seq == expectedSeqNum) {
        ov match {
          case Some(v) => kv += (k -> v)
          case None => kv -= k
        }
        //using seq as id here
        persistActor ! Persist(k,ov,seq)
        retryPersist += seq -> (sender -> Persist(k,ov,seq))
        context.system.scheduler.scheduleOnce(1120.millisecond, self, PersistTimeout(seq))
        context.system.scheduler.scheduleOnce(100.millisecond, self, RetryPersist(seq))
      }
      else if (seq < expectedSeqNum) {
        sender ! SnapshotAck(k, seq)
      }
      else {
        //do nothing
      }
    case RetryPersist(seq) =>
      retryPersist.get(seq) match {
        case Some((_,prst)) =>
          persistActor ! prst
          context.system.scheduler.scheduleOnce(100.millisecond, self, RetryPersist(seq))
        case None => ()
      }
    case PersistTimeout(seq) =>
      retryPersist.get(seq) match {
        //simply removing it from retry map will cause the 100ms trigger to
        //not try again after next 100ms
        case Some(_) => retryPersist -= seq
        case None => ()
      }
    case Persisted(k,seq) =>
      retryPersist.get(seq) match {
        case Some((actorRef, Persist(k,_,seq))) =>
          actorRef ! SnapshotAck(k,seq)
          expectedSeqNum += 1
          retryPersist -= seq
        case None => ()
      }
  }
}

