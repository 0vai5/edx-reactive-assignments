package kvstore

import akka.actor.{Actor, ActorRef, Cancellable, Props, ReceiveTimeout}

import scala.concurrent.duration._

object Replicator {
  case class Replicate(key: String, valueOption: Option[String], id: Long)
  case class Replicated(key: String, id: Long)
  
  case class Snapshot(key: String, valueOption: Option[String], seq: Long)
  case class SnapshotAck(key: String, seq: Long)

  case class IsAcked(seq: Long)
  case object CleanUp

  def props(replica: ActorRef): Props = Props(new Replicator(replica))
}

class Replicator(val replica: ActorRef) extends Actor {
  import Replicator._
  import context.dispatcher
  
  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */

  // map from sequence number to pair of sender and request
  var acks = Map.empty[Long, (ActorRef, Replicate)]
  // a sequence of not-yet-sent snapshots (you can disregard this if not implementing batching)
  var pending = Vector.empty[Snapshot]

  var cancellables = Map.empty[Long, Cancellable]

  var _seqCounter = 0L
  def nextSeq() = {
    val ret = _seqCounter
    _seqCounter += 1
    ret
  }

  
  /* TODO Behavior for the Replicator. */
  def receive: Receive = {
    case msg@Replicate(k, ov, _) =>
      val seq = nextSeq()
      acks += (seq -> (sender -> msg))
      replica ! Snapshot(k, ov, seq)
      cancellables += seq -> context.system.scheduler.scheduleOnce(200.millisecond, self, IsAcked(seq))
    case SnapshotAck(_, seq) =>
      acks.get(seq) match {
        case Some((replicaSender, Replicate(k, _, id))) =>
          replicaSender ! Replicated(k, id)
          acks -= seq
        case _ => ()
      }
    case IsAcked(seq) =>
      cancellables -= seq
      acks.get(seq) match {
        case Some((_, Replicate(k, ov, seq))) =>
          replica ! Snapshot(k, ov, seq)
          cancellables += seq -> context.system.scheduler.scheduleOnce(200.millisecond, self, IsAcked(seq))
        case None => ()
      }
    case CleanUp =>
      cancellables foreach {case (_,c) => c.cancel()}
      context.stop(self)
  }

}
